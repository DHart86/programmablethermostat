package com.mycompany.programmablethermostat;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import java.util.ArrayList;

import java.io.FileReader; 
import java.util.Iterator; 
import java.util.Map; 
  
import org.json.simple.JSONArray; 
import org.json.simple.JSONObject; 
import org.json.simple.parser.*;

public class Thermo {
    public static void main(String[] args) throws Exception {
       String info = jsonGetRequest("http://media.capella.edu/BBCourse_Production/IT4774/temperature.json");
      // System.out.println(info);
         
    //CLASS DRIVER FOR UNIT INFORMATION DISPLAY     
    class Identity {   
        String ident, nam, thermT, utcT;
        Identity(String a, String b, String c, String d) {
        ident = a;
        nam = b;
        thermT = c;
        utcT = d;
        }
            
        public String displayIdentity(){
            return ident;
        }    
            
        public String displayName() {
            return nam;
        }
            
        public String displayThermostatTime() {
            return thermT;
        }
            
        public String displayUTCTime() {
            return utcT;
        }
    } 
    
    //CLASS DRIVER FOR TEMPERATURE AND HUMIDITY DISPLAY
    class Run {
        int temp, humid;
        Run(int a,int b) {
            temp = a;
            humid = b;
        }
        
        public int displayTemp() {
            return temp;
        }
        
        public int displayHumidity() {
            return humid;
        }
    }   
    
    ///CLASS DRIVER FOR STATUS CODE DISPLAYS
    class Status {
        int code;
        String statusMessage;
        Status(int a,String b) {
            code = a;
            statusMessage = b;
        }
        
        public int displayCode() {
            return code;
        }
        
        public String displayStatus() {
            if (statusMessage.length() < 1) {
                return "Idle";
            }
            
            else return statusMessage;
        }
    }

       
       // Object from JSON string
       //////////////////////////////////     
        Object obj = new JSONParser().parse(info); 
          
        JSONObject thermDetails = (JSONObject) obj;         
                String identifier = (String) thermDetails.get("identifier"); 
                String name = (String) thermDetails.get("name");
                String thermTime = (String) thermDetails.get("thermostatTime");
                String utcTime = (String) thermDetails.get("utcTime");
                
                Map runtime = ((Map)thermDetails.get("runtime")); 
                Map status = (Map)thermDetails.get("status");
                        ////ADDRESS NESTED OBJECTS RUNTIME/STATUS
                        ArrayList<String> templist = new ArrayList(2);     
                         Iterator<Map.Entry> iter = runtime.entrySet().iterator(); 
                            while (iter.hasNext()) { 
                                Map.Entry pair = iter.next(); 
                                //System.out.println(/*pair.getKey() + " : " + */pair.getValue());
                                templist.add(pair.getValue().toString());
                            }                                                      
                               ///Convert String humidity and temps to int
                               int intHumidity = Integer.parseInt(templist.get(0));
                               int intTemp = Integer.parseInt(templist.get(1));
                           
                        ArrayList<String> statuslist = new ArrayList(2);     
                         Iterator<Map.Entry> iter2 = status.entrySet().iterator();
                            while (iter2.hasNext()) {
                                Map.Entry pair = iter2.next();                               
                                statuslist.add(pair.getValue().toString());
                            }
                            int intCode = Integer.parseInt(statuslist.get(0));
                            String statMess = statuslist.get(1);

 
   Identity myTherm = new Identity(identifier, name, thermTime, utcTime);
    System.out.println(myTherm.displayIdentity() + " " + myTherm.displayName());
    System.out.println(myTherm.displayThermostatTime() + " " + myTherm.displayUTCTime());
    
   Run myThermNumbers = new Run(intTemp, intHumidity) ;
    System.out.println("The temp is " + myThermNumbers.displayTemp() + " and the humidity is " + myThermNumbers.displayHumidity() );
    
   Status myThermStatus = new Status(intCode, statMess) ;
    System.out.println("The status is " + myThermStatus.displayStatus() + " and code: " + myThermStatus.displayCode());
}
    
    
     
    
  private static String streamToString(InputStream inputStream) {
     String text = new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
     
     return text;
   }

  public static String jsonGetRequest(String urlQueryString) {
    String json = null;
    try {
      URL url = new URL(urlQueryString);
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      
      ////REQUEST STATUS
      String statusResponse = connection.getResponseMessage();
      int statusCode = connection.getResponseCode();
      System.out.println("Request Status = " + statusCode + ": " + statusResponse);
      //////
      
      InputStream jsonIn = connection.getInputStream();
      json = streamToString(jsonIn); // input stream to string
    } catch (IOException ex) {
    }
    return json;
    }
    
}
