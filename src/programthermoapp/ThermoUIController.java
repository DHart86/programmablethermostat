package programthermoapp;

import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import org.json.simple.parser.*;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import java.util.ArrayList;

import java.util.Iterator;
import java.util.Map;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.PrintWriter;


import org.json.simple.JSONObject;


public class ThermoUIController implements Initializable {

    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private Label label1;
    @FXML
    private TextField currTemp;
    @FXML
    private TextField currSett;
    @FXML
    private TextField currTime;
    @FXML
    private TextField currHumid;
    @FXML
    private Button upBtn;
    @FXML
    private Button dwnBtn;
    @FXML
    private Button resetBtn;
    @FXML
    private Label label3;
    @FXML
    private Button switchToDays;
    @FXML
    private AnchorPane overlay;
    @FXML
    private TextField pane2Setting;
    @FXML
    private TextField pane2Day;
    @FXML
    private Button increaseButtonDay;
    @FXML
    private Button decreaseButtonDay;
    @FXML
    private TextField pane2Time;
    @FXML
    private Button increaseButtonTime;
    @FXML
    private Button decreaseButtonTime;
    @FXML
    private Button saveButton;
    @FXML
    private Button clearButton;
    @FXML
    private TextField pane2HYear;
    @FXML
    private TextField pane2hDay;
    @FXML
    private TextField pane2HMonth;
    @FXML
    private Label p2HDayLabel;
    @FXML
    private Label p2HMonthLabel;
    @FXML
    private Label p2HYearLabel;
    @FXML
    private Button increaseHMonth;
    @FXML
    private Button decreaseHMonth;
    @FXML
    private Button increaseHYear;
    @FXML
    private Button increaseHDay;
    @FXML
    private Button decreaseHYear;
    @FXML
    private Button decreaseHDay;
    
    
    
    @FXML
    public void swithToDays() {
        overlay.setVisible(true);
    }

    @FXML
    public void switchToMain(MouseEvent event) {
        overlay.setVisible(false);
    }

    @FXML
    public void increaseDay() {
        if (SaveDay.getDay() < 8) {
            pane2Day.setText(SaveDay.increaseDay());
        }
        else pane2Day.setText(SaveDay.getDayString());
    }

    @FXML
    public void decreaseDay() {
        if (SaveDay.getDay() > 0) {
        pane2Day.setText(SaveDay.decreaseDay());
        }
        
        pane2hDay.setVisible(false);
             pane2HMonth.setVisible(false);
             pane2HYear.setVisible(false);
             p2HDayLabel.setVisible(false);
             p2HMonthLabel.setVisible(false);
             p2HYearLabel.setVisible(false);
             decreaseHDay.setVisible(false);
             increaseHDay.setVisible(false);
             decreaseHMonth.setVisible(false);
             increaseHMonth.setVisible(false);
             decreaseHYear.setVisible(false);
             increaseHYear.setVisible(false);
             
             holSetting.setHDay(0);
        
    }
    
    @FXML
    public void increaseTime() {
        if (SaveDay.getTime() < 23) {
        pane2Time.setText(SaveDay.increaseTime());
        }
    }
    
    @FXML
    public void decreaseTime() {
        if (SaveDay.getTime() > 0) {
        pane2Time.setText(SaveDay.decreaseTime());
        }
    }
    
    @FXML
    public void clearButton() throws Exception{
        try (PrintWriter writer = new PrintWriter("src/programthermoapp/saveSettings.txt")) {
            writer.print("");
            System.out.println("Settings Reset!");
        }
        
             pane2hDay.setVisible(false);
             pane2HMonth.setVisible(false);
             pane2HYear.setVisible(false);
             p2HDayLabel.setVisible(false);
             p2HMonthLabel.setVisible(false);
             p2HYearLabel.setVisible(false);
             decreaseHDay.setVisible(false);
             increaseHDay.setVisible(false);
             decreaseHMonth.setVisible(false);
             increaseHMonth.setVisible(false);
             decreaseHYear.setVisible(false);
             increaseHYear.setVisible(false);
        
                          holSetting.setHDay(0);

    }
    
    
    @FXML
    public void increaseHDay() {
        if (holSetting.getHDay() < 31 ) {
        pane2hDay.setText(holSetting.increaseHDay());
        }
    }
    
    @FXML
    public void decreaseHDay() {
        if (holSetting.getHDay() > 1) {
        pane2hDay.setText(holSetting.decreaseHDay());
        }
    }
    
    @FXML
    public void increaseHMonth() {
        if (holSetting.getHMonth() < 11 ) {
        pane2HMonth.setText(holSetting.increaseHMonth());
        }
    }
    
    @FXML
    public void decreaseHMonth() {
        if (holSetting.getHMonth() > 0) {
        pane2HMonth.setText(holSetting.decreaseHMonth());
        }
    }
    
    @FXML
    public void increaseHYear() {
        if (holSetting.getHYear() < 2 ) {
        pane2HYear.setText(holSetting.increaseHYear());
        }
    }
    
    @FXML
    public void decreaseHYear() {
        if (holSetting.getHYear() > 0) {
        pane2HYear.setText(holSetting.decreaseHYear());
        }
    }
   

    public class currentSetting {

        int defaultTemp;

        currentSetting(int a) {
            defaultTemp = a;
        }

        public int increaseTemp() {
            return defaultTemp++;
        }

        public int decreaseTemp() {
            return defaultTemp--;
        }

        public int setTemp(int a) {
            defaultTemp = a;
            return a;
        }

        public int getTemp() {
            return defaultTemp;
        }
    }

    currentSetting setSetting = new currentSetting(68);
    controlsDayTime SaveDay = new controlsDayTime(0,0);
    UserSettings set1 = new UserSettings();
    controlsHolidayTime holSetting = new controlsHolidayTime(0,0,0);

    
    
     String userDay = SaveDay.getDayString();
     String userHour = SaveDay.getTimeString();
     String userTemp = Integer.toString(setSetting.getTemp());
     

    /////////
    //Setting Controls
    @FXML
    public void increaseTemp() {
        if (setSetting.getTemp() < 90) {
            currSett.setText(Integer.toString(setSetting.increaseTemp()));

        } else {
            currSett.setText(Integer.toString(setSetting.getTemp()));
        }
    }
    
    @FXML
    public void increaseTemp2() {
        if (setSetting.getTemp() < 90) {
            setSetting.increaseTemp();
            pane2Setting.setText(Integer.toString(setSetting.getTemp()));
        } else {
            currSett.setText(Integer.toString(setSetting.getTemp()));
        }
    }

    @FXML
    public void decreaseTemp() {
        if (setSetting.getTemp() > 50) {
            currSett.setText(Integer.toString(setSetting.decreaseTemp()));

        } else {
            currSett.setText(Integer.toString(setSetting.getTemp()));
        }
    }
    
    @FXML
    public void decreaseTemp2() {
        if (setSetting.getTemp() > 50) {
            setSetting.decreaseTemp();
            pane2Setting.setText(Integer.toString(setSetting.getTemp()));

        } else {
            currSett.setText(Integer.toString(setSetting.getTemp()));
        }
    }

    @FXML
    public void resetSetting() {
        setSetting.setTemp(68);
        currSett.setText(Integer.toString(setSetting.getTemp()));
    }   
    
    
    @FXML
    public void saveSettingsButton() {
        
        
////User Settings
//////////////
 if (SaveDay.getDay() < 8) {
     String userDay = SaveDay.getDayString();
     String userHour = SaveDay.getTimeString();
     String userTemp = Integer.toString(setSetting.getTemp());
        
        set1.setDay("Temperature", userTemp);
        set1.setDay("Hour", userHour);
        set1.setDay("Day", userDay);
 
    System.out.println(userTemp + " at " + userHour + " on " + userDay + " saved successfully.");
    }
 
 
  else if (SaveDay.getDay() == 8 && holSetting.getHDay() > 0) {
     
     String userHDay = holSetting.getHDayString();
     String userHMonth = holSetting.getHMonthString();
     String userHYear = holSetting.getHYearString();
     set1.setDay("Temperature", userTemp);
     set1.setDay("Day", userHDay);
     set1.setDay("Month", userHMonth);
     set1.setDay("Year", userHYear);
     System.out.println(userTemp + " on the " + userHDay + " of " + userHMonth + " in " + userHYear + " saved successfully - Happy Holidays");
 }
 
 else if (SaveDay.getDay() == 8 && holSetting.getHDay() == 0) {
     
             pane2hDay.setVisible(true);
             pane2HMonth.setVisible(true);
             pane2HYear.setVisible(true);
             p2HDayLabel.setVisible(true);
             p2HMonthLabel.setVisible(true);
             p2HYearLabel.setVisible(true);
             decreaseHDay.setVisible(true);
             increaseHDay.setVisible(true);
             decreaseHMonth.setVisible(true);
             increaseHMonth.setVisible(true);
             decreaseHYear.setVisible(true);
             increaseHYear.setVisible(true);
             

             pane2HYear.setText(holSetting.getHYearString());
             pane2HMonth.setText(holSetting.getHMonthString());
             holSetting.increaseHDay();
             pane2hDay.setText(holSetting.getHDayString());

             System.out.println("Enter holiday settings");

 }
 

 
    }
////End Setting Controls
/////
    
    
    @FXML
    public void handleButtonAction() throws Exception {

        String info = jsonGetRequest("http://media.capella.edu/BBCourse_Production/IT4774/temperature.json");
        // System.out.println(info);

        //CLASS DRIVER FOR UNIT INFORMATION DISPLAY     
        class Identity {

            String ident, nam, thermT, utcT;

            Identity(String a, String b, String c, String d) {
                ident = a;
                nam = b;
                thermT = c;
                utcT = d;
            }

            public String displayIdentity() {
                return ident;
            }

            public String displayName() {
                return nam;
            }

            public String displayThermostatTime() {
                return thermT;
            }

            public String displayUTCTime() {
                return utcT;
            }
        }

        //CLASS DRIVER FOR TEMPERATURE AND HUMIDITY DISPLAY
        class Run {
                
            double temp, humid;

            Run(double a, double b) {
                temp = a;
                humid = b;
            }

            public double displayTemp() {
                return temp;
            }

            public double displayHumidity() {
                return humid;
            }
        }

        ///CLASS DRIVER FOR STATUS CODE DISPLAYS
        class Status {

            int code;
            String statusMessage;

            Status(int a, String b) {
                code = a;
                statusMessage = b;
            }

            public int displayCode() {
                return code;
            }

            public String displayStatus() {
                if (statusMessage.length() < 1) {
                    return "Idle";
                } else {
                    return statusMessage;
                }
            }
        }

        // Object from JSON string
        //////////////////////////////////     
        Object obj = new JSONParser().parse(info);

        JSONObject thermDetails = (JSONObject) obj;
        String identifier = (String) thermDetails.get("identifier");
        String name = (String) thermDetails.get("name");
        String thermTime = (String) thermDetails.get("thermostatTime");
        String utcTime = (String) thermDetails.get("utcTime");

        Map runtime = ((Map) thermDetails.get("runtime"));
        Map status = (Map) thermDetails.get("status");
        ////ADDRESS NESTED OBJECTS RUNTIME/STATUS
        ArrayList<String> templist = new ArrayList(2);
        Iterator<Map.Entry> iter = runtime.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry pair = iter.next();
            //System.out.println(/*pair.getKey() + " : " + */pair.getValue());
            templist.add(pair.getValue().toString());
        }
        ///Convert String humidity and temps to int
        int intHumidity = Integer.parseInt(templist.get(0));
        int intTemp = Integer.parseInt(templist.get(1));

        ArrayList<String> statuslist = new ArrayList(2);
        Iterator<Map.Entry> iter2 = status.entrySet().iterator();
        while (iter2.hasNext()) {
            Map.Entry pair = iter2.next();
            statuslist.add(pair.getValue().toString());
        }
        int intCode = Integer.parseInt(statuslist.get(0));
        String statMess = statuslist.get(1);

        Identity myTherm = new Identity(identifier, name, thermTime, utcTime);
        //System.out.println(myTherm.displayIdentity() + " " + myTherm.displayName());
        //System.out.println(myTherm.displayThermostatTime() + " " + myTherm.displayUTCTime());

        Run myThermNumbers = new Run(intTemp, intHumidity);
        //System.out.println("The temp is " + myThermNumbers.displayTemp() + " and the humidity is " + myThermNumbers.displayHumidity());

        Status myThermStatus = new Status(intCode, statMess);
        //System.out.println("The status is " + myThermStatus.displayStatus() + " and code: " + myThermStatus.displayCode());
        
        
        String pattern = /*"EEEEE dd MMMMM yyyy*/ "HH:mm:ss a";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        String date = simpleDateFormat.format(new Date());
         
        
        currTemp.setText(Double.toString(myThermNumbers.displayTemp()));
        currHumid.setText(Double.toString(myThermNumbers.displayHumidity()));
        currSett.setText(Integer.toString(setSetting.getTemp()));
        currTime.setText(date);     
        pane2Setting.setText(Integer.toString(setSetting.getTemp()));
        
        pane2Day.setText("Monday");
        pane2Time.setText("12AM");
        
        
    }

    private static String streamToString(InputStream inputStream) {
        String text = new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
        return text;
    }

    public String jsonGetRequest(String urlQueryString) {
        String json = null;
        try {
            URL url = new URL(urlQueryString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            ////REQUEST STATUS
            String statusResponse = connection.getResponseMessage();
            int statusCode = connection.getResponseCode();
            //System.out.println("Request Status = " + statusCode + ": " + statusResponse);
            //////

            InputStream jsonIn = connection.getInputStream();
            json = streamToString(jsonIn); // input stream to string
        } catch (IOException ex) {
        }

        return json;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

}
