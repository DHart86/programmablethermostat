/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programthermoapp;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Properties;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.Writer;
import java.io.Reader;


public class UserSettings {
    
   Path settingsFile = Paths.get("src/programthermoapp/saveSettings.txt");
   public final Properties Day = new Properties();
   //Write File
   public void setDay(String key, String value){

    Day.setProperty(key, value);
   
        try {
        Writer storeDay = Files.newBufferedWriter(settingsFile);
        Day.store(storeDay, "User Settings"); 
        }
        catch(IOException Ex) {
        System.out.println("Error :" +
        Ex.getMessage());
        }
       
   }   
   //Read File
    public void getDay(String temp, String hour, String day){
        try {      
          Reader daySettings = 
            Files.newBufferedReader(settingsFile);
            Day.load(daySettings);
            System.out.println("The temperature should be " + Day.getProperty(temp) + " on the " + Day.getProperty(hour) + "th hour of " + Day.getProperty(day));
        }
        catch(IOException Ex)
        {
            System.out.println("Error :" +
                    Ex.getMessage());
        }    
    }    
}
