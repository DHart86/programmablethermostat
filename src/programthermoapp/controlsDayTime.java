
package programthermoapp;

public class controlsDayTime {
    
    String[] dayArray = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday", "Weekend", "Holiday"};
    String[] timeArray = {"12AM", "1AM","2AM","3AM","4AM","5AM","6AM","7AM",
        "8AM","9AM","10AM","11AM","12AM","1AM","2PM","3PM","4PM","5PM","6PM","7PM",
        "8PM","9PM","10PM","11PM","12PM"};
       public int dayToSave;
       public int timeToSave;
       
     public controlsDayTime(int a, int b) {
            dayToSave = a;
            timeToSave = b;
        }
     
      String increaseDay() {
          dayToSave++;
            return dayArray[dayToSave];
        }

        public String decreaseDay() {
            dayToSave--;
            return dayArray[dayToSave];
        }

        public int getDay() {
            return dayToSave;
        }
        
        public String getDayString() {
            return dayArray[dayToSave];
        }    
        
        
        
        public String increaseTime() {
          timeToSave++;
            return timeArray[timeToSave];
        }

        public String decreaseTime() {
            timeToSave--;
            return timeArray[timeToSave];
        }


        public int getTime() {
            return timeToSave;
        }
        
        public String getTimeString() {
            return timeArray[timeToSave];
        }       
}
