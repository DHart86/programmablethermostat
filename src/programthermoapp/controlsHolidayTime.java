
package programthermoapp;

public class controlsHolidayTime {
    
    int holidayDay = 0;    
    String[] holidayMonth = {"January", "February","March","April","May","June","July","August","September","October","November","December"};
    String[] holidayYear = {"2018", "2019", "2020"};
    
       public int hDayToSave;
       public int hMonthToSave;
       public int hYearToSave;
       
     public controlsHolidayTime(int a, int b, int c) {
         hDayToSave = a;
         hMonthToSave = b;
         hYearToSave = c;
     }
    
     
        public int setHDay(int a) {
            holidayDay = a;
            return holidayDay;
        }
     
        public String increaseHDay() {
          holidayDay++;
            return Integer.toString(holidayDay);
        }

        public String decreaseHDay() {
            holidayDay--;
            return Integer.toString(holidayDay);
        }

        public int getHDay() {
            return holidayDay;
        }
        
        public String getHDayString() {
            return Integer.toString(holidayDay);
        }    
        
        
        
        public String increaseHMonth() {
          hMonthToSave++;
            return holidayMonth[hMonthToSave];
        }

        public String decreaseHMonth() {
            hMonthToSave--;
            return holidayMonth[hMonthToSave];
        }


        public int getHMonth() {
            return hMonthToSave;
        }
        
        public String getHMonthString() {
            return holidayMonth[hMonthToSave];
        }
    
        
        
         public String increaseHYear() {
          hYearToSave++;
            return holidayYear[hYearToSave];
        }

        public String decreaseHYear() {
            hYearToSave--;
            return holidayYear[hYearToSave];
        }


        public int getHYear() {
            return hYearToSave;
        }
        
        public String getHYearString() {
            return holidayYear[hYearToSave];
        }
    
}
